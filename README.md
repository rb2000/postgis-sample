#  Sample Spring Boot REST api project using POSTGIS
This is a simple sample project showing how to user the hibernate spatial artifact 
with Spring Boot and postgis.  As example a REST endpoint is provided to fetch
all points inside a radius , by latitude/longitude.

This backend uses a Docker image to run a PostgreSQL instance with postgis extensions.   
Stack:  Java 8, Spring Boot 2.5.2, OpenAPI 3.0, Postgis
The API docs can be tested at the API UI: ( http://localhost:8080/swagger-ui )

 
 

## Configuration
 All settings can be changed in the files application.yml and docker-compose.yml
 
## Database Model
There is only one entity in the database, with latitude  logitude fields as Double, and
a Point field. The Point is created from the lat/lon values.

```java
  @Column(nullable = false)
  private Double longitude;

  @Column(nullable = false)
  private Double latitude;

  @JsonSerialize(using = GeometrySerializer.class)
  @JsonDeserialize(contentUsing = GeometryDeserializer.class)
  @Column(columnDefinition = "geometry(Point,4326)")
  private Point location;
  
```

 ## Running the app
 Call  first
`docker-compose down -v &&  docker-compose up`
to start the docker container.

After that you can call 
`mvn spring-boot:run `
to start the web application

## Usage
You can go to the swagger endpoint and query the endpoints from there, or use CURL.

We use the endpoint /api/exampleEntity/byCoordinates/{latitude}/{longitude}/{radiusInMeters}
to get all entities that are inside a circle centered at latitude/longitude with radius "radiusInMeters".

To test the app, we have to insert 2 points in the database using the POST endpoint:
`curl -X POST "http://localhost:8080/api/exampleEntity" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"id\":0,\"longitude\":13.475807353375178,\"latitude\":52.47170554789445}"`

![post_1](/uploads/0a2a3f3ca952acf213e291e661c23cd6/post_1.png)

The server responds with the table id (long) of the created entity on the server:

![reponse_post_1](/uploads/f8eca552d78163d4276e29ae85f7d5e8/reponse_post_1.png)


Now we add another ExampleEntity to the database, this time with a different  pair of coordinates 
` curl -X POST "http://localhost:8080/api/exampleEntity" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"id\":0,\"longitude\":13.476622744938622,\"latitude\":52.46906185708572}"`
 ![post_2](/uploads/f9ed3742decdfc1743fb37bea908f665/post_2.png)

If we make a request to the GET endpoint, we should receive the 2 ExampleEntity objects:
`curl -X GET "http://localhost:8080/api/exampleEntity" -H  "accept: application/json"`


Now that we have the 2 points in the database, we can call the utility method "measureDistance"
in order to get the distance between the 2 points, and  we get 0.002766579927258496 as result. So we have to multiply it by 100000, as the points are in reality about 300 meters apart (see the map below) 

`http://localhost:8080/api/exampleEntity/measureDistance/1/2`

![measure_wrong](/uploads/fa91c53596a0b424d7c34751436fc6d9/measure_wrong.png)

Real distance:

![map](/uploads/d33b636fe17286a79575a5204e6af8d6/map.png)


That is why in we divide the radius value by 100000 at the method 
    geometricShapeFactory.setSize((radiusInMeters / 100000.0) * 2);
when creating the circle filter.


The ExampleEntity objets are about 276.5 meters apart:
/api/exampleEntity/byCoordinates/{latitude}/{longitude}/{radiusInMeters}

 

So querying the endpoint with one of the saved entities as center and a radius of 276 meters 
for the circle will should return only the same point at the center of the circle:
`curl -X GET "http://localhost:8080/api/exampleEntity/byCoordinates/52.46906185708572/13.476622744938622/276" -H  "accept: application/json"`

![query_1](/uploads/7d5cf3a9b77e287a1c9ea26ccfca9149/query_1.png)


If we increase the radius by one meter the circle will contain the second saved entity, as 
they are 276.5 meters apart and our circle has  now a radius of 277 meters:
![query_2](/uploads/1f84394481b5f7a2d018974bfce70638/query_2.png)




