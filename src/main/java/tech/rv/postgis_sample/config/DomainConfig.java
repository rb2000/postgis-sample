package tech.rv.postgis_sample.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EntityScan("tech.rv.postgis_sample.domain")
@EnableJpaRepositories("tech.rv.postgis_sample.repos")
@EnableTransactionManagement
public class DomainConfig {}
