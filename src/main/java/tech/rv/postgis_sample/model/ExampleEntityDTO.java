package tech.rv.postgis_sample.model;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.locationtech.jts.geom.Point;

@Getter
@Setter
public class ExampleEntityDTO {

  private Long id;

  @NotNull private Double longitude;

  @NotNull private Double latitude;
}
