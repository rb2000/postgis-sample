package tech.rv.postgis_sample.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FieldError {

  private String field;
  private String errorCode;
}
