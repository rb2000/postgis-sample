package tech.rv.postgis_sample.repos;

import java.util.List;
import org.locationtech.jts.geom.Geometry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tech.rv.postgis_sample.domain.ExampleEntity;

public interface ExampleEntityRepository extends JpaRepository<ExampleEntity, Long> {

  @Query("SELECT ee FROM ExampleEntity ee WHERE  WITHIN(ee.location, :filter) = true")
  List<ExampleEntity> findAllNearby(@Param("filter") Geometry filter);
}
