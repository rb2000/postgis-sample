package tech.rv.postgis_sample.rest;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import tech.rv.postgis_sample.model.ExampleEntityDTO;

@ApiResponses(
    value = {
      @ApiResponse(
          responseCode = "200",
          description = "Request OK",
          content = {
            @Content(
                mediaType = "application/json",
                schema = @Schema(implementation = ExampleEntityDTO.class))
          }),
      @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
      @ApiResponse(
          responseCode = "401",
          description = "Wrong credentials",
          content = @Content(schema = @Schema(hidden = true))),
      @ApiResponse(
          responseCode = "403",
          description = "You do not have privileges " + " to see this information",
          content = @Content(schema = @Schema(hidden = true))),
      @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content),
      @ApiResponse(
          responseCode = "412",
          description = "One or more conditions in the request header fields evaluated to false",
          content = @Content),
      @ApiResponse(
          responseCode = "500",
          description = "A generic error occurred on the server",
          content = @Content),
      @ApiResponse(responseCode = "503", description = "Service Unavailable ", content = @Content)
    })

public interface ExampleEntityApiDocController {}
