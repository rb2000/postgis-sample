package tech.rv.postgis_sample.rest;

import java.util.List;
import javax.validation.Valid;
import org.locationtech.jts.geom.Geometry;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import tech.rv.postgis_sample.domain.ExampleEntity;
import tech.rv.postgis_sample.model.ExampleEntityDTO;
import tech.rv.postgis_sample.service.ExampleEntityService;

@RestController
@RequestMapping(value = "/api/exampleEntity", produces = MediaType.APPLICATION_JSON_VALUE)
public class ExampleEntityController implements ExampleEntityApiDocController {

  private final ExampleEntityService exampleEntityService;

  public ExampleEntityController(final ExampleEntityService exampleEntityService) {
    this.exampleEntityService = exampleEntityService;
  }

  @GetMapping
  public ResponseEntity<List<ExampleEntityDTO>> getAllExampleEntities() {
    return ResponseEntity.ok(exampleEntityService.findAll());
  }

  @GetMapping("/{id}")
  public ResponseEntity<ExampleEntityDTO> getExampleEntity(@PathVariable final Long id) {
    return ResponseEntity.ok(exampleEntityService.get(id));
  }

  @GetMapping("/measureDistance/{idOne}/{idTwo}")
  public ResponseEntity<Double> measureDistance(@PathVariable final Long idOne,@PathVariable final Long idTwo) {
    return ResponseEntity.ok(exampleEntityService.measureDistance(idOne,idTwo));
  }

  @GetMapping("/byCoordinates/{latitude}/{longitude}/{radiusInMeters}")
  public ResponseEntity<List<ExampleEntityDTO>> findAllNearby(
      @PathVariable Double latitude,
      @PathVariable Double longitude,
      @PathVariable Double radiusInMeters) {
    Geometry circle = exampleEntityService.createCircle(latitude, longitude, radiusInMeters);
     return ResponseEntity.ok(exampleEntityService.findAllNearby(circle));
  }

  @PostMapping
  public ResponseEntity<Long> createExampleEntity(
      @RequestBody @Valid final ExampleEntityDTO exampleEntityDTO) {
    return new ResponseEntity<>(exampleEntityService.create(exampleEntityDTO), HttpStatus.CREATED);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Void> updateExampleEntity(
      @PathVariable final Long id, @RequestBody @Valid final ExampleEntityDTO exampleEntityDTO) {
    exampleEntityService.update(id, exampleEntityDTO);
    return ResponseEntity.ok().build();
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteExampleEntity(@PathVariable final Long id) {
    exampleEntityService.delete(id);
    return ResponseEntity.noContent().build();
  }
}
