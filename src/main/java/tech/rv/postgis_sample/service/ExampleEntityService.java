package tech.rv.postgis_sample.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.PrecisionModel;
import org.locationtech.jts.util.GeometricShapeFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;
import tech.rv.postgis_sample.domain.ExampleEntity;
import tech.rv.postgis_sample.model.ExampleEntityDTO;
import tech.rv.postgis_sample.repos.ExampleEntityRepository;

@Service
public class ExampleEntityService {

  private final ExampleEntityRepository exampleEntityRepository;
  private GeometricShapeFactory geometricShapeFactory = new GeometricShapeFactory();

  public ExampleEntityService(final ExampleEntityRepository exampleEntityRepository) {
    this.exampleEntityRepository = exampleEntityRepository;
  }

  public List<ExampleEntityDTO> findAll() {
    return exampleEntityRepository.findAll().stream()
        .map(exampleEntity -> mapToDTO(exampleEntity, new ExampleEntityDTO()))
        .collect(Collectors.toList());
  }

  public ExampleEntityDTO get(final Long id) {
    return exampleEntityRepository
        .findById(id)
        .map(exampleEntity -> mapToDTO(exampleEntity, new ExampleEntityDTO()))
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  }

  public List<ExampleEntityDTO> findAllNearby(Geometry filter) {
    return Optional.of(
            exampleEntityRepository.findAllNearby(filter).stream()
                .map(exampleEntity -> mapToDTO(exampleEntity, new ExampleEntityDTO()))
                .collect(Collectors.toList()))
        .orElseGet(Collections::emptyList);
  }

  public Long create(final ExampleEntityDTO exampleEntityDTO) {
    final ExampleEntity exampleEntity = new ExampleEntity();
    mapToEntity(exampleEntityDTO, exampleEntity);
    return exampleEntityRepository.save(exampleEntity).getId();
  }

  public void update(final Long id, final ExampleEntityDTO exampleEntityDTO) {
    final ExampleEntity exampleEntity =
        exampleEntityRepository
            .findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    mapToEntity(exampleEntityDTO, exampleEntity);
    exampleEntityRepository.save(exampleEntity);
  }

  public void delete(final Long id) {
    exampleEntityRepository.deleteById(id);
  }

  private ExampleEntityDTO mapToDTO(
      final ExampleEntity exampleEntity, final ExampleEntityDTO exampleEntityDTO) {
    exampleEntityDTO.setId(exampleEntity.getId());
    exampleEntityDTO.setLongitude(exampleEntity.getLongitude());
    exampleEntityDTO.setLatitude(exampleEntity.getLatitude());
    return exampleEntityDTO;
  }

  private ExampleEntity mapToEntity(
      final ExampleEntityDTO exampleEntityDTO, final ExampleEntity exampleEntity) {
    Point point = createPoint(exampleEntityDTO.getLatitude(), exampleEntityDTO.getLongitude());
    exampleEntity.setLongitude(exampleEntityDTO.getLongitude());
    exampleEntity.setLatitude(exampleEntityDTO.getLatitude());
    exampleEntity.setLocation(point);
    return exampleEntity;
  }

  /**
   * here we need to divide the radius in meters by 100000, otherwise it won't work
   *
   * @param latitude
   * @param longitude
   * @param radiusInMeters
   * @return
   */
  public Geometry createCircle(Double latitude, Double longitude, Double radiusInMeters) {
    Coordinate circleCentre = new Coordinate(longitude, latitude);
    geometricShapeFactory.setNumPoints(32);
    geometricShapeFactory.setCentre(circleCentre);
    geometricShapeFactory.setSize((radiusInMeters / 100000.0) * 2);
    Geometry circle = geometricShapeFactory.createCircle();
    circle.setSRID(4326);
    return circle;
  }


  public Point createPoint(Double latitude, Double longitude) {
    PrecisionModel precisionModel = new PrecisionModel(PrecisionModel.maximumPreciseValue);
    GeometryFactory geoFactory = new GeometryFactory(precisionModel, 4326);
    Coordinate coordinate = new Coordinate(longitude, latitude);
    return geoFactory.createPoint(coordinate);
  }

  /** will return a vale that needs to be multiplied by 100000 to be converted to meters
   *
   * @param pointOneId
   * @param pointTwoId
   * @return
   */
  public Double measureDistance(Long pointOneId, Long pointTwoId) {
    Point pointOne = exampleEntityRepository.findById(pointOneId).get().getLocation();
    Point pointTwo = exampleEntityRepository.findById(pointTwoId).get().getLocation();
    return  pointOne.distance(pointTwo);
  }
}
